public class Jackpot{
	public static void main(String[] args){
	
	System.out.println("Welcome TO The Jackpot");
	
	Board board = new Board();
	boolean gameOver = false;
	int numOfTilesClosed = 0;
	
	while(!gameOver){
		System.out.println(board.toString());
		
		boolean result = board.playATurn();	

		if (result){
			gameOver = true;
		}else{
			numOfTilesClosed++;
		}
	}
	
	if(numOfTilesClosed >= 7){
		System.out.println("Congratulations! You reached the jackpot and won.");
	}else {
            System.out.println("Sorry, you lost. Better luck next time!");
        }
		
		System.out.println("Game over! You closed " + numOfTilesClosed + " tiles.");
	}
}