public class Board{
	
	private Die first;
	private Die second;
	private boolean[] tiles;
	
	//constructor 
	public Board(){
		this.first = new Die();
		this.second = new Die();
		this.tiles = new boolean [12];
		for(int i=0 ; i<this.tiles.length; i++){
			this.tiles[i]=false;
		}
		
	}
	
	//Override toString
	public String toString(){
		String dice = "";
		for(int i = 0; i<this.tiles.length; i++){
			if (!this.tiles[i]){
				dice += (i+1) + "  ";
			}else{
				dice += "X " + " ";
			}
		}
		return dice;
	}
	
	//playATurn Method No input returns boolean 
	public boolean playATurn(){
		
		this.first.roll();
		this.second.roll();
		
		System.out.println(this.first.toString() + " , " + this.second.toString());
		
		int sumOfDice = this.first.getFaceValue() + this.second.getFaceValue();
		boolean gameOver = false;
		
		if (this.tiles[sumOfDice-1]){
			System.out.println("Shut");
			if(this.tiles[first.getFaceValue()-1]){
				System.out.println("first dice already closed");
				if(this.tiles[second.getFaceValue()-1]){
				System.out.println("All the tiles for these values are already shut");
				gameOver=true;
				}else{
				System.out.println("Closing tile with the same value as die two+");
				this.tiles[second.getFaceValue()-1]=true;
				gameOver=false;
				}
			}else{
				System.out.println("Closing tile with the same value as die one");
				this.tiles[first.getFaceValue()-1]=true;
				gameOver = false;
			}
	}else{
		this.tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			gameOver = false;
	}
	return gameOver;
	}
}