import java.util.Random;
public class Die{
	
	private int faceValue;
	private Random random;
	
	//constructer 
	public Die(){
		this.faceValue=2;
		this.random = new Random();
	}
	
	//getter 
	public int getFaceValue(){
		return this.faceValue;
	}
	
	//to set the value of faceValue to a random number
	public void roll(){
		this.faceValue = random.nextInt(6)+1;
	}
	
	//toString
	public String toString(){
		//String a = "";
		//a += this.faceValue;
		return "faceValue: " + this.faceValue;
	}
}